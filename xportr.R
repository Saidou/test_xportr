
rm(list = ls())

# Loading packages
library(dplyr)
library(labelled)
library(xportr)
library(admiral)
library(rlang)
library(readxl)

# Loading in our example data
adsl <- admiral::admiral_adsl

system.file(paste0("specs/", "ADaM_admiral_spec.xlsx"), package = "xportr")

var_spec <- 
  read_xlsx(system.file(paste0("specs/", "ADaM_admiral_spec.xlsx"), package = "xportr"),
            sheet = "Variables") %>%
  rename(type = "Data Type") %>%
  set_names(tolower)

adsl_type <- xportr_type(adsl, var_spec, domain = "ADSL", verbose = "message")

adsl_length <- adsl %>% xportr_length(var_spec, domain = "ADSL", "message")

adsl_order <- xportr_order(adsl, var_spec, domain = "ADSL", verbose = "message")

adsl_fmt <- adsl %>% xportr_format(var_spec, domain = "ADSL")

adsl_lbl <- adsl %>% xportr_label(var_spec, domain = "ADSL", "message")

adsl %>%
  xportr_type(var_spec, "ADSL", "message") %>%
  xportr_length(var_spec, "ADSL", "message") %>%
  xportr_label(var_spec, "ADSL", "message") %>%
  xportr_order(var_spec, "ADSL", "message") %>%
  xportr_format(var_spec, "ADSL") %>%
  xportr_write("adsl.xpt", label = "Subject-Level Analysis Dataset")

